package api

import (
	"gin-blog/app/controllers/api"
	v1 "gin-blog/app/controllers/api/v1"
	"gin-blog/router/middleware/jwt"
	"github.com/gin-gonic/gin"
)

func Routes(r *gin.Engine) {
	r.GET("", func(c *gin.Context) {
		c.JSON(200, gin.H{
			"message": "welcome!",
		})
	})

	r.GET("/test", func(c *gin.Context) {
		c.JSON(200, gin.H{
			"message": "test",
		})
	})

	r.POST("/api/auth", api.GetAuth)
	r.POST("/api/upload", api.UploadFile)

	apiv1 := r.Group("/api/v1")

	apiv1.Use(jwt.JWT())
	{
		//获取标签列表
		apiv1.GET("/tags", v1.GetTags)
		//新建标签
		apiv1.POST("/tags", v1.AddTag)
		//更新指定标签
		apiv1.PUT("/tags", v1.EditTag)
		//删除指定标签
		apiv1.DELETE("/tags/:id", v1.DeleteTag)

		// 文章列表
		apiv1.GET("/articles", v1.GetArticles)
		// 文章详情
		apiv1.GET("/articles/:id", v1.GetArticle)
		// 添加文章
		apiv1.POST("/articles", v1.AddArticle)
		// 更新文章
		apiv1.PUT("/articles/:id", v1.UpdageArticle)
		// 删除文章
		apiv1.DELETE("articles/:id", v1.DeleteArticle)
	}
}
