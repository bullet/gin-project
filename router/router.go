package router

import (
	apiroute "gin-blog/router/api"
	webroute "gin-blog/router/web"
	"gin-blog/utils/upload"
	"github.com/gin-gonic/gin"
	"net/http"

	"gin-blog/utils/setting"
)

func InitRouter() *gin.Engine  {
	// 两种实例化gin的方式

	// 第一种方式,用默认的
	//r := gin.Default()

	// 第二种方式,自定义
	r := gin.New()
	r.Use(gin.Logger())
	r.Use(gin.Recovery())

	gin.SetMode(setting.ServerSetting.RunMode)

	// StaticFS 是加载一个完整的目录资源,StaticFile 是加载单个文件
	r.StaticFS("/upload/images", http.Dir(upload.GetImageFullPath()))	// 上传的图片加载
	r.StaticFS("/public", http.Dir("./resources/public"))	// 静态资源加载
	r.StaticFile("/favicon.ico", "./resources/favicon.ico")

	apiroute.Routes(r)
	webroute.Routes(r)

	return r
}
