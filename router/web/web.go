package web

import (
	"gin-blog/app/controllers/web"
	"github.com/gin-gonic/gin"
)

func Routes(r *gin.Engine) {
	// 导入模板根目录下所有的文件
	r.LoadHTMLGlob("app/view/*/*/*")
	r.GET("/index.html", web.Index)
}