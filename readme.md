##项目结构
 ```cassandraql
├─app  
│  ├─controllers    控制器
│  │  ├─api         api控制器
│  │  │  └─v1       版本
│  │  └─web         web控制器
│  ├─models         模型
│  ├─repositories   业务处理
│  ├─services       逻辑处理
│  └─view           视图/模板
│      └─web        web控制器的模板
│          └─index  index控制的模板
├─conf              项目配置
├─databases         数据库驱动
│  └─mysql          mysql驱动
├─resources         静态资源
│  ├─public         前端资源
|  └─upload         上传资源
├─router            路由
│  ├─api            api控制器的路由
│  ├─middleware     所有中间件
│  │  ├─cors        跨域中间件
│  │  └─jwt         jwt中间件
│  └─web            web控制器的路由
├─runtime           应用运行时数据
│  └─logs           日志文件
└─utils             封装的助手方法/类
    ├─errcode       错误码
    ├─helper        其他助手
    ├─logging       日志助手
    ├─setting       项目配置设置
    └─upload        上传助手
```

##初始项目数据库
新建blog数据库，编码为utf8_general_ci 在blog数据库下，新建以下表  
1、标签表  
```cassandraql
CREATE TABLE `blog_tag` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL DEFAULT '' COMMENT '标签名称',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '修改时间',
  `author` varchar(20) NOT NULL DEFAULT '' COMMENT '创建人',
  `state` tinyint(3) unsigned NOT NULL DEFAULT '1' COMMENT '状态 0为禁用、1为启用',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='文章标签管理';

```
2、 文章表  
```cassandraql
CREATE TABLE `blog_article` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `tag_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '标签ID',
  `title` varchar(50) NOT NULL DEFAULT '' COMMENT '文章标题',
  `desc` varchar(255) NOT NULL DEFAULT '' COMMENT '简述',
  `content` text,
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '修改时间',
  `author` varchar(20) NOT NULL DEFAULT '' COMMENT '创建人',
  `state` tinyint(3) unsigned NOT NULL DEFAULT '1' COMMENT '状态 0为禁用1为启用',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='文章管理';

```
3、 认证表  
```cassandraql
CREATE TABLE `blog_auth` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(50) DEFAULT '' COMMENT '账号',
  `password` varchar(50) DEFAULT '' COMMENT '密码',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
INSERT INTO `blog`.`blog_auth` (`id`, `username`, `password`) VALUES (null, 'test', 'test123456');
```