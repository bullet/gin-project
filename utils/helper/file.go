package helper

import (
	"io/ioutil"
	"mime/multipart"
	"os"
	"path"
)

// 获取文件大小
func GetSize(f multipart.File) (int, error) {
	content , err := ioutil.ReadAll(f)

	return len(content), err
}

// 获取文件后缀
func GetExt(filename string) string {
	return path.Ext(filename)
}

// 检查文件是否存在
func CheckExist(filepath string) bool {
	_, err := os.Stat(filepath)

	return os.IsNotExist(err)
}

// 检查文件权限
func CheckPermission(filepath string) bool {
	_, err := os.Stat(filepath)

	return os.IsPermission(err)
}

// 不存在则新建文件夹
func IsNotExistMKDir(filepath string) error {
	if exist := CheckExist(filepath); exist == false {
		if err := MKDir(filepath); err != nil {
			return err
		}
	}
	return nil
}

// 新建文件夹
func MKDir(filepath string) error {
	err := os.MkdirAll(filepath, os.ModePerm)
	if err != nil {
		return err
	}

	return nil
}

// 打开文件
func Open(filepath string, flag int, perm os.FileMode) (*os.File, error) {
	f, err := os.OpenFile(filepath, flag, perm)
	if err != nil {
		return nil, err
	}
	return f, nil
}