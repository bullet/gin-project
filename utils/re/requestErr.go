package re

import (
	"fmt"
	"gin-blog/utils/logging"
	"github.com/astaxie/beego/validation"
)

func RequestErrors(errors []*validation.Error) (msg string) {
	for _, err := range errors {
		logging.Info(err.Key, err.Message)
		msg = fmt.Sprintf("参数%s, %s", err.Key, err.Message)
		break
	}
	return
}