package re

import (
	"gin-blog/utils/errcode"
	"github.com/gin-gonic/gin"
)

type Gin struct {
	C *gin.Context
}

func (g *Gin) Response(httpCode, errCode int, msg string, data interface{}) {
	if msg != "" {
		g.C.JSON(httpCode, gin.H{
			"code": httpCode,
			"msg":  msg,
			"data": data,
		})
	} else {
		g.C.JSON(httpCode, gin.H{
			"code": httpCode,
			"msg":  errcode.GetMsg(errCode),
			"data": data,
		})
	}

	return
}