package models

type Model struct {
	ID int `gorm:"primary_key;AUTO_INCREMENT" json:"id"`
	CreateTime string `gorm:"column:create_time" json:"create_time"`
	UpdateTime string `gorm:"column:update_time" json:"update_time"`
}