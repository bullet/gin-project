package models

type Tag struct {
	Model

	Name string `gorm:"default:''; type:varchar(100); not null" json:"name"`
	Author string `gorm:"default:''; type:varchar(20); not null" json:"author"`
	State int `gorm:"default:1; type:tinyint; not null" json:"state"`
}