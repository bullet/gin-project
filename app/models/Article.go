package models

type Article struct {
	Model

	TagId int `gorm:"default:0; type:int; not null; index" json:"tag_id"`
	Tag Tag `json:"tag"`

	Title string `gorm:"default:''; type:varchar(50); not null" json:"title"`
	Desc string `gorm:"default:''; type:varchar(255); not null" json:"desc"`
	Content string `gorm:"type:text" json:"content"`
	Author string `gorm:"default:''; type:varchar(20); not null" json:"author"`
	ImageCover string `gorm:"default:''; type:varchar(255); not null; comment:'封面链接'" json:"image_cover"`
	State int `gorm:"default:1; type:tinyint; not null" json:"state"`
}