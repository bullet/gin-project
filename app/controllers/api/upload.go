package api

import (
	"gin-blog/utils/errcode"
	"gin-blog/utils/logging"
	"gin-blog/utils/upload"
	"github.com/gin-gonic/gin"
	"net/http"
)

func UploadFile(c *gin.Context) {
	code := errcode.SUCCESS
	data := make(map[string]string)
	file, image, err := c.Request.FormFile("image")

	if err != nil {
		logging.Warn(err)
		code = errcode.ERROR
		c.JSON(http.StatusOK, gin.H{
			"code": code,
			"msg":  errcode.GetMsg(code),
			"data": data,
		})
	}
	if image == nil {
		code = errcode.INVALID_PARAMS
	} else {
		imageName := upload.GetImageName(image.Filename)
		fullPath := upload.GetImageFullPath()
		savePath := upload.GetImagePath()
		src := fullPath + imageName
		if ! upload.CheckImageExt(imageName) || ! upload.CheckImageSize(file) {
			code = errcode.ERROR_UPLOAD_CHECK_IMAGE_FORMAT
		} else {
			err := upload.CheckImage(fullPath)
			if err != nil {
				logging.Warn(err)
				code = errcode.ERROR_UPLOAD_CHECK_IMAGE_FAIL
			} else if err := c.SaveUploadedFile(image, src); err != nil {
				logging.Warn(err)
				code = errcode.ERROR_UPLOAD_SAVE_IMAGE_FAIL
			} else {
				data["image_url"] = upload.GetImageFullUrl(imageName)
				data["image_save_url"] = savePath + imageName
			}
		}
	}
	c.JSON(http.StatusOK, gin.H{
		"code": code,
		"msg":  errcode.GetMsg(code),
		"data": data,
	})
}