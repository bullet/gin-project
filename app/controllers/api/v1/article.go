package v1

import (
	repo "gin-blog/app/repositories"
	"gin-blog/app/validation/article"
	"gin-blog/utils/errcode"
	"gin-blog/utils/helper"
	"gin-blog/utils/re"
	"gin-blog/utils/setting"
	"github.com/gin-gonic/gin"
	"github.com/unknwon/com"
	"net/http"
)

// 获取文章列表
func GetArticles(c *gin.Context)  {
	var resp = re.Gin{C: c}
	var data = make(map[string]interface{})
	var maps = make(map[string]interface{})
	code := errcode.SUCCESS
	maps["state"] = 1

	data["list"] = repo.GetArticles(helper.GetPage(c), setting.AppSetting.PageSize)
	data["count"] = repo.GetArticleTotal(maps)

	resp.Response(http.StatusOK, code, "", data)
}

// 获取文章详情
func GetArticle(c *gin.Context)  {
	var msg string
	var resp = re.Gin{C: c}
	id := com.StrTo(c.Param("id")).MustInt()
	valid := article.GetArticleValidation(c)
	code := errcode.INVALID_PARAMS

	if valid.HasErrors() {
		msg = re.RequestErrors(valid.Errors)
		resp.Response(http.StatusOK, code, msg, make(map[string]string))
		return
	}

	data := repo.GetArticle(id)
	code = errcode.SUCCESS

	resp.Response(http.StatusOK, code, msg, data)
}

// 更新文章
func UpdageArticle(c *gin.Context)  {
	var resp = re.Gin{C:c}
	id := com.StrTo(c.Param("id")).MustInt()
	code := errcode.INVALID_PARAMS

	if ! repo.ExistArticleByID(id) {
		resp.Response(http.StatusOK, code, "不存在此文章", make(map[string]string))
		return
	}

	var msg string
	var data = make(map[string]interface{})
	title := c.PostForm("title")
	desc := c.PostForm("desc")
	content := c.PostForm("content")
	author := c.PostForm("author")
	tagId := c.PostForm("tag_id")

	valid := article.UpdageArticleValidation(c)

	if  valid.HasErrors() {
		msg = re.RequestErrors(valid.Errors)
		resp.Response(http.StatusOK, code, msg,  make(map[string]string))
		return
	}

	data["title"] = title
	data["desc"] = desc
	data["content"] = content
	data["author"] = author
	if tagId != "" {
		data["tag_id"] = com.StrTo(tagId).MustInt()
	}

	article := repo.GetArticle(id)
	if article.Title != title {
		if repo.GetAritcleByTitle(title).ID > 0 {
			resp.Response(http.StatusOK, code, "存在同名标题", make(map[string]string))
			return
		}
		repo.EditAritcle(id, data)
		code = errcode.SUCCESS
		resp.Response(http.StatusOK, code, "", make(map[string]string))
	} else {
		repo.EditAritcle(id, data)
		code = errcode.SUCCESS
		resp.Response(http.StatusOK, code, "", make(map[string]string))
	}
}

// 新增文章
func AddArticle(c *gin.Context)  {
	var resp = re.Gin{C:c}
	var msg string
	code := errcode.INVALID_PARAMS
	title := c.PostForm("title")
	desc := c.PostForm("desc")
	content := c.PostForm("content")
	author := c.PostForm("author")
	tagId := com.StrTo(c.PostForm("tag_id")).MustInt()

	valid := article.AddArticleValidation(c)

	if valid.HasErrors() {
		msg = re.RequestErrors(valid.Errors)
		resp.Response(http.StatusOK, code, msg,  make(map[string]string))
		return
	}

	article := repo.GetAritcleByTitle(title)
	if article.ID > 0 {
		resp.Response(http.StatusOK, code, "标题重复了",  make(map[string]string))
		return
	}

	code = errcode.SUCCESS
	data := make(map[string]interface{})
	data["title"] = title
	data["desc"] = desc
	data["content"] = content
	data["author"] = author
	data["tag_id"] = tagId
	repo.AddArticle(data)
	resp.Response(http.StatusOK, code, "",  make(map[string]string))
}

// 删除文章
func DeleteArticle(c *gin.Context)  {
	var resp = re.Gin{C:c}
	id := com.StrTo(c.Param("id")).MustInt()

	repo.DeleteArticle(id)

	resp.Response(http.StatusOK, errcode.SUCCESS, "", make(map[string]string))
}