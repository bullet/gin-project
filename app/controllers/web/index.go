package web

import (
	"github.com/gin-gonic/gin"
	"net/http"
)

func Index(c *gin.Context)  {
	c.HTML(http.StatusOK, "index.html", gin.H{
		"Title" : "Powered By Gin",
		"Content" : "欢迎拥抱Gin框架,一个轻量级高定制框架!",
	})
}
