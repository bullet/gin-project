package repositories

import (
	"gin-blog/app/models"
	"gin-blog/databases/mysql"
)

func CheckAuth(username, password string) bool {
	var auth models.Auth
	mysql.DB.Select("id").Where(models.Auth{Username:username, Password:password}).First(&auth)

	if auth.ID > 0 {
		return true
	}
	return false
}