package repositories

import (
	"gin-blog/app/models"
	"gin-blog/databases/mysql"
)

// 获取tags
func GetTags(pageNum int, pageSize int, maps interface{}) (tags []models.Tag) {
	mysql.DB.Where(maps).Offset(pageNum).Limit(pageSize).Find(&tags)

	return
}

// 获取tag
func GetTag(name string) (tag []models.Tag) {
	mysql.DB.Where("name = ?", name).Take(&tag)

	return
}

// 统计tags数量
func GetTagsTotal(maps interface{}) (count int) {
	mysql.DB.Model(&models.Tag{}).Where(maps).Count(&count)
	return
}

// 增加tag
func AddTag(name string, author string) bool {
	mysql.DB.Create(&models.Tag{
		Name:   name,
		Author: author,
	})

	return true
}

func ExistTagByName(name string) bool {
	var count int
	mysql.DB.Model(&models.Tag{}).Where("name = ?", name).Count(&count)
	if count != 0 {
		return true
	}
	return false
}

func DeleteTag(id int) bool {
	mysql.DB.Where("id = ?", id).Delete(&models.Tag{})
	return true
}

func EditTag(id int, data interface {}) bool {
	mysql.DB.Model(&models.Tag{}).Where("id = ?", id).Updates(data)
	return true
}
