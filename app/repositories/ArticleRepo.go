package repositories

import (
	"gin-blog/app/models"
	"gin-blog/databases/mysql"
)

// 判断文章是否存在
func ExistArticleByID(id int) bool  {
	var article models.Article
	mysql.DB.Where("id = ?", id).First(&article)
	if article.ID > 0 {
		return true
	}
	return false
}

// 获取文章数量
func GetArticleTotal(maps interface{}) (count int)  {
	mysql.DB.Model(&models.Article{}).Where(maps).Count(&count)

	return
}

// 获取文章列表
func GetArticles(pageNum int, pageSize int) (articles []models.Article) {
	mysql.DB.Preload("Tag").Offset(pageNum).Limit(pageSize).Find(&articles)

	return
}

// 获取文章详情
func GetArticle(id int) (article models.Article) {
	mysql.DB.Where("id = ?", id).First(&article)
	mysql.DB.Model(&article).Related(&article.Tag)

	return
}

// 根据标题获取文章详情
func GetAritcleByTitle(title string) (article models.Article) {
	mysql.DB.Where("title = ?", title).First(&article)

	return
}

// 编辑文章
func EditAritcle(id int, data interface{}) bool {
	mysql.DB.Model(&models.Article{}).Where("id = ?", id).Updates(data)

	return true
}

// 增加文章
func AddArticle(data map[string]interface{}) bool {
	mysql.DB.Create(&models.Article{
		TagId: data["tag_id"].(int),
		Title: data["title"].(string),
		Desc: data["desc"].(string),
		Content: data["content"].(string),
		Author: data["author"].(string),
	})

	return true
}

// 删除文章
func DeleteArticle(id int) bool {
	mysql.DB.Where("id = ?", id).Delete(&models.Article{})

	return true
}