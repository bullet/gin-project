package article

import (
	"github.com/astaxie/beego/validation"
	"github.com/gin-gonic/gin"
	"github.com/unknwon/com"
)

func GetArticleValidation(c *gin.Context) validation.Validation {
	id := com.StrTo(c.Param("id")).MustInt()
	valid := validation.Validation{}
	valid.Required(id, "id").Message("不能为空")
	return valid
}

func AddArticleValidation(c *gin.Context) validation.Validation {
	title := c.PostForm("title")
	desc := c.PostForm("desc")
	content := c.PostForm("content")
	author := c.PostForm("author")

	valid := validation.Validation{}

	valid.Required(title, "title").Message("不能为空")
	valid.MaxSize(title, 50, "title").Message("不能超过50个字符的长度")
	valid.Required(desc, "desc").Message("不能为空")
	valid.MaxSize(desc, 255, "desc").Message("不能超过255个字符的长度")
	valid.Required(content, "content").Message("不能为空")
	valid.Required(author, "author").Message("不能为空")

	return valid
}

func UpdageArticleValidation(c *gin.Context) validation.Validation {
	title := c.PostForm("title")
	desc := c.PostForm("desc")
	content := c.PostForm("content")
	author := c.PostForm("author")

	valid := validation.Validation{}

	valid.Required(title, "title").Message("不能为空")
	valid.MaxSize(title, 50, "title").Message("不能超过50个字符的长度")
	valid.Required(desc, "desc").Message("不能为空")
	valid.MaxSize(desc, 255, "desc").Message("不能超过255个字符的长度")
	valid.Required(content, "content").Message("不能为空")
	valid.Required(author, "author").Message("不能为空")

	return valid
}
